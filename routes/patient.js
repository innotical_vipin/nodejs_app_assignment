const express = require('express');
const router = express.Router();
const patientController = require('../controllers/patient');

router.get('/getpatient/:id', patientController.getPatientById, function (req, res) {
    console.log("requset", req.session.key)
    res.render('welcome', {
        info: req.info
    })
})
router.get('/getPatients', patientController.getPatients, function (req, res) {
    if (req.success) {
        res.render('userList', {
            info: req.info
        });
    } else {
        res.render('doctorProfile', {
            title: 'Menu',
            message: req.flash('message'),
            info: req.session.key
        });
    }
})

module.exports = router
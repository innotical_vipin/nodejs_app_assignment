const express = require('express');
const router = express.Router();
const authController = require('../controllers/authentication');

router.post('/signup', authController.signup, function (req, res) {
    console.log("req", req.flash('message'))
    if (req.success) {
        res.render('welcome')
    } else {
        res.render('signup', {
            title: 'Menu',
            message: req.flash('message')
        });
    }
})
router.post('/login', authController.login, function (req, res) {
    console.log("req", req.flash('message'))
    if (req.success) {
        console.log("req.user", req.user)
        console.log("req.session", req.session.key)
        if (req.user.userType == "Doctor") {
            res.locals.user = req.session.key;
            res.render('doctorProfile', { info: req.user })
        }
        else if (req.user.userType == "Admin") {
            res.locals.user = req.session.key;
            res.render('adminProfile', { info: req.user })
        }
       else if (req.user.userType == "Patient") {
            res.locals.user = req.session.key;
            res.render('patientProfile', { info: req.user })
        } else {
            let array = ['Doctor', 'Admin', 'Patient']
            res.render('login', {
                info: array
            })
        }
    } else {
        res.render('login', {
            title: req.flash('message'),
            message: req.flash('message')
        });
    }
})
router.get('/login', authController.login, function (req, res) {
    console.log("session msns", req.session.key)
    if (req.session.key) {
        if (req.session.key.userType == "Admin") {
            res.render('adminProfile', {
                info: req.session.key
            })
        }
        else if (req.session.key.userType == "Doctor") {
            res.render('doctorProfile', {
                info: req.session.key
            })
        }
        else if (req.session.key.userType == "Patient") {
            res.render('patientProfile', {
                info: req.session.key
            })
        }
        else {
            let array = ['Doctor', 'Admin', 'Patient']
            res.render('login', {
                info: array
            })
        }
    } else {
        let array = ['Doctor', 'Admin', 'Patient']
        res.render('login', {
            info: array
        })
    }
})
router.get('/signup', authController.login, function (req, res) {
    let array = ['Doctor', 'Admin', 'Patient']
    res.render('signup', {
        info: array
    })
})
router.get('/profile', function (req, res) {

    res.render('welcome')
})
router.get('/logout', function (req, res) {
    req.session.destroy();
    let array = ['Doctor', 'Admin', 'Patient']
    res.redirect('/auth/login')
})


module.exports = router;

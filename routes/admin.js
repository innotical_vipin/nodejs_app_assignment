const express = require('express');
const router = express.Router();
const adminController = require('../controllers/admin');
const doctorController = require('../controllers/doctor');
const patientController = require('../controllers/patient');

router.get('/getadmin', adminController.getAdminById, function (req, res) {
    console.log("requset", req.session.key)
    if (req.success) {
        res.render('adminProfile', { info: req.info })
    } else {
        let array = ['Doctor', 'Admin', 'Patient']
        res.render('login', {
            info: array
        })
    }
})
router.get('/getAdmins', adminController.getAdmins, function (req, res) {
    if (req.success) {
        res.render('userList', {
            info: req.info
        });
    } else {
        let array = ['Doctor', 'Admin', 'Patient']
        res.render('login', {
            info: array
        })
    }
})
router.get('/get/:userType/:id', function (req, res) {
    if (req.params.userType == "Patient") {
        patientController.getPatientByAdmin(req, (err, doctor) => {
            if (req.success) {
                res.render('editUser', {
                    info: req.info
                });
            } else {
                res.redirect('/patient/getPatients')
            }

        })
    } else if (req.params.userType == "Doctor") {
        doctorController.getDoctorByAdmin(req, (err, doctor) => {
            if (req.success) {
                res.render('editUser', {
                    info: req.info
                });
            } else {
                res.redirect('/doctor/getDoctors')
            }
        })
    } else {
        let array = ['Doctor', 'Admin', 'Patient']
        res.render('login', {
            info: array
        })
    }
})
router.post('/update/:userType/:id', function (req, res) {
    if (req.params.userType == "Patient") {
        patientController.updateInfo(req, (err, doctor) => {
            if (req.success) {
                res.redirect('/patient/getPatients')
            } else {
                res.redirect(`/admin/get/${req.params.userType}/${req.params.id}`)
            }
        })
    } else if (req.params.userType == "Doctor") {
        doctorController.updateInfo(req, (err, doctor) => {
            console.log("req.success",req.success)
            if (req.success) {
                res.redirect('/doctor/getDoctors')
            } else {
                res.redirect(`/admin/get/${req.params.userType}/${req.params.id}`)
            }
        })
    } else {
        let array = ['Doctor', 'Admin', 'Patient']
        res.render('login', {
            info: array
        })
    }
})

module.exports = router
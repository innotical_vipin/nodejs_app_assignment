const express = require('express');
const router = express.Router();
const doctorController = require('../controllers/doctor');

router.get('/getdoctor/:id', doctorController.getDoctorById, function (req, res) {
    console.log("requset",req.session.key)
    res.render('doctorProfile', {
        info: req.info
    })
})

router.get('/getDoctors', doctorController.getDoctors, function (req, res) {
    if (req.success) {
        res.render('userList', {
            info: req.info
        });
    } else {
        let array = ['Doctor', 'Admin', 'Patient']
        res.render('login', {
            info: array
        })
    }
})
module.exports=router
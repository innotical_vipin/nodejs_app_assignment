
let authRoutes = require('./authentication');
let doctor = require('./doctor');
let patient = require('./patient');
let chat = require('./chat');
let admin = require('./admin')
module.exports = function (app) {
    app.use('/auth', authRoutes);
    app.use('/doctor', doctor);
    app.use('/patient', patient);
    app.use('/chat', chat)
    app.use('/admin', admin)
}
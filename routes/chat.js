const express = require('express');
const router = express.Router();
const chatController = require('../controllers/chat')
router.get('/history', chatController.getUserData, function (req, res) {
    console.log("req.session", req.session.key)
    if (req.session.key) {
        if (req.success) {
            console.log("infp", req.info)
            res.render('chat', { info: req.info, info2: req.info2 })
        } else {
            let array = ['Doctor', 'Admin', 'Patient']
            res.render('login', {
                info: array
            })
        }
    } else {
        let array = ['Doctor', 'Admin', 'Patient']
        res.render('login', {
            info: array
        })
    }
})

module.exports = router
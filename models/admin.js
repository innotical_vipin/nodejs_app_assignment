const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


var adminSchema = new Schema({
    email: { type: String, required: true },
    firstName: { type: String, required: false },
    lastName: { type: String, required: false },
    password: { type: String, required: false },
    isActive: { type: Boolean, required: false, default: true },
    userType: { type: String }
},
    {
        timestamps: { createdAt: 'createdAt', lastUpdated: 'lastUpdated' }
    });


/**
 * method for create user access token
 * with expires limit
 */
adminSchema.statics.encode = (data, callback) => {
    jwt.sign(data, "tokensecret", { expiresIn: 5000 }, function (err, data) {
        callback(err, data)
    });
};
/**
 * feth user detail from access token
 * @check also token is valid or not
 */
adminSchema.statics.decode = (token) => {
    try {
        var decoded = jwt.verify(token, config.token.TOKEN_SECRET_A);
        return decoded;
    } catch (err) {
        return false;
    }
}
/**
 * generate hash password
 */
adminSchema.methods.generateHash = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(9));
}
/**
 * compare hash password
 */
adminSchema.methods.comparePassword = function (passwordToCompare, callback) {
    bcrypt.compare(passwordToCompare, this.password, function (err, isMatch) {
        callback(err, isMatch);
    });
};

module.exports = mongoose.model('Admin', adminSchema);

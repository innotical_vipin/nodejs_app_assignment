const mongoose = require('mongoose');
const Schema = mongoose.Schema;


var patientSchema = new Schema({
    email: { type: String, required: true },
    firstName: { type: String, required: false },
    lastName: { type: String, required: false },
    password: { type: String, required: false },
    isActive: { type: Boolean, required: false, default: true },
    userType: { type: String }

},
    {
        timestamps: { createdAt: 'createdAt', lastUpdated: 'lastUpdated' }
    });



module.exports = mongoose.model('patient', patientSchema);

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const chatController = require('../controllers/chat')

var chatSchema = new Schema({
    doctorId: { type: String },
    patientId: { type: String },
    adminId: { type: String },
    msg: { type: String },
    name: { type: String }
},
    {
        timestamps: { createdAt: 'createdAt', lastUpdated: 'lastUpdated' }
    });



module.exports = mongoose.model('chat', chatSchema);

$(function () {
    //make connection
    var socket = io.connect('http://localhost:8000')

    //buttons and inputs
    var message = $("#message")
    var username = $("#username")
    var send_message = $("#send_message")
    var send_username = $("#send_username")
    var chatroom = $("#chatroom")
    var feedback = $("#feedback")
    var userType = $("#userType")
    var id = $("#id")
    var name = $("#name")
    var senderType = $("#senderType")
    var senderId = $("#senderId")
    socket.on('connect', function () {
        console.log('check 2', socket.connected);
    });
    //Emit message
    function sendMessage(data) {
        console.log("data is here", data)
    }
    send_message.click(function () {
        console.log("data", message.val(), userType.val(), name.val())
        socket.emit('new_message', { senderId: senderId.val(), senderType: senderType.val(), message: message.val(), userType: userType.val(), name: name.val(), id: id.val() })
        chatroom.append("<p class='message' style='font-size:20px;'>" + name.val() + ": " + message.val() + "</p>")
        message.val('');
        feedback.html('');
    })

    //Listen on new_message
    socket.on("new_message", (data) => {
        data.username = "vipin sharma"
        feedback.html('');
        message.val('');
        chatroom.append("<p class='message' style='font-size:20px;'>" + data.username + ": " + data.message + "</p>")
    })

    //Emit a username
    send_username.click(function () {
        socket.emit('change_username', { username: username.val() })
    })

    //Emit typing
    message.bind("keypress", () => {
        socket.emit('typing')
    })

    //Listen on typing
    socket.on('typing', (data) => {
        feedback.html("<p><i>" + data.username + " is typing a message..." + "</i></p>")
    })
});
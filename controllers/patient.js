const Patient = require('../models/patient')

let patientController = {}

patientController.getPatientById = (req, res, next) => {
    Patient.findOne({ _id: req.params.id }, (err, patient) => {
        if (err) {
            req.success = false
            next(null, req.flash('message', 'Internal server error'));
        } else {
            req.success = true
            req.info = patient;
            next(null, patient)
        }
    })
}
patientController.getPatients = (req, res, next) => {
    Patient.find({}, (err, patient) => {
        if (err) {
            req.success = false
            next(null, req.flash('message', 'Internal server error'));
        } else {
            req.success = true
            req.info = patient;
            next(null, patient)
        }
    })
}


patientController.updateInfo = (req, next) => {
    if (!req.body.userType) {
        req.success = false
        next(null, req.flash('message', 'Enter type of user'))
    }
    else if (!req.body.firstName) {
        req.success = false
        next(null, req.flash('message', 'firstName is required'))
    }
    else if (!req.body.lastName) {
        req.success = false
        next(null, req.flash('message', 'lastName is required'))
    }
    else if (!req.body.email) {
        req.success = false
        next(null, req.flash('message', 'email is required'))
    } else {
        Patient.update({ _id: req.params.id }, { $set: req.body }, (err, update) => {
            if (err) {
                req.success = false
                next(null, req.flash('message', 'Internal server error'));
            } else {
                req.success = true
                req.info = update;
                next(null, update)
            }
        })
    }
}

patientController.getPatientByAdmin = (req, next) => {
    Patient.findOne({ _id: req.params.id }, (err, patient) => {
        if (err) {
            req.success = false
            next(null, req.flash('message', 'Internal server error'));
        } else {
            req.success = true
            req.info = patient;
            next(null, patient)
        }
    })


}

module.exports = patientController
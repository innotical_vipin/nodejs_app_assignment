const Chat = require('../models/chat');
const Doctor = require('../models/doctor');
const Patient = require('../models/patient')
const Admin = require('../models/admin')
let async=require('async')
let chatController = {}



chatController.createPost = (data, next) => {
    var chat = new Chat(data)
    chat.save((err, chat) => {
        next(err, chat)
    })
}

chatController.getUserData = (req, res, next) => {
    if (req.query.Doctor) {
        async.parallel({
            one: function (callback) {
                Doctor.findOne({ _id: req.query.Doctor }, (err, doctor) => {
                    callback(err, doctor)
                })
            },
            two: function (callback) {
                let condition = {
                    doctorId: req.query.Doctor
                }
                if (req.session.key.userType == "Admin")
                    condition.adminId = req.session.key._id
                if (req.session.key.userType == "Doctor")
                    condition.doctorId = req.session.key._id
                if (req.session.key.userType == "Patient")
                    condition.patientId = req.session.key._id

                Chat.find(condition, (err, data) => {
                    callback(err, data)
                })
            }
        }, function (err, results) {
            if (err) {
                req.success = false
                next(null, req.flash('message', 'Internal server error'));
            } else {
                req.success = true
                req.info = results.one
                req.info2 = results.two
                next(null, results)
            }
        });
    } else if (req.query.Patient) {
        async.parallel({
            one: function (callback) {
                Patient.findOne({ _id: req.query.Patient }, (err, doctor) => {
                    callback(err, doctor)
                })
            },
            two: function (callback) {
                let condition = {
                    patientId: req.query.Patient
                }
                if (req.session.key.userType == "Admin")
                    condition.adminId = req.session.key._id
                if (req.session.key.userType == "Doctor")
                    condition.doctorId = req.session.key._id
                if (req.session.key.userType == "Patient")
                    condition.patientId = req.session.key._id

                Chat.find(condition, (err, data) => {
                    callback(err, data)
                })
            }
        }, function (err, results) {
            if (err) {
                req.success = false
                next(null, req.flash('message', 'Internal server error'));
            } else {
                req.success = true
                req.info = results.one
                req.info2 = results.two
                next(null, results)
            }
        });
    } else if (req.query.Admin) {
        async.parallel({
            one: function (callback) {
                Admin.findOne({ _id: req.query.Admin }, (err, doctor) => {
                    callback(err, doctor)
                })
            },
            two: function (callback) {
                let condition = {
                    adminId: req.query.Admin
                }
                if (req.session.key.userType == "Admin")
                    condition.adminId = req.session.key._id
                if (req.session.key.userType == "Doctor")
                    condition.doctorId = req.session.key._id
                if (req.session.key.userType == "Patient")
                    condition.patientId = req.session.key._id

                Chat.find(condition, (err, data) => {
                    callback(err, data)
                })
            }
        }, function (err, results) {
            if (err) {
                req.success = false
                next(null, req.flash('message', 'Internal server error'));
            } else {
                req.success = true
                req.info = results.one
                req.info2 = results.two
                next(null, results)
            }
        });
    }else{
        req.success = false
        next(null)
    }
}

module.exports = chatController
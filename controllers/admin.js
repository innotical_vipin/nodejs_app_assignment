const Admin = require('../models/admin')


let adminController = {}

adminController.getAdminById = (req, res, next) => {
    if (req.session.key) {
        Admin.findOne({ _id: req.session.key._id }, (err, admin) => {
            if (err) {
                req.success = false
                next(null, req.flash('message', 'Internal server error'));
            } else {
                req.success = true
                req.info = admin;
                next(null, admin)
            }
        })
    } else {
        req.success = false
        next(null)
    }

}

adminController.getAdmins = (req, res, next) => {
    console.log("req.session", req.session.key)
    if (req.session.key) {
            Admin.find({}, (err, admin) => {
                if (err) {
                    req.success = false
                    next(null, req.flash('message', 'Internal server error'));
                } else {
                    req.success = true
                    req.info = admin;
                    next(null, admin)
                }
            })
    }
    else {
        req.success = false
        next(null)
    }
}





module.exports=adminController
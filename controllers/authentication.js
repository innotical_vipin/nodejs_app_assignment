const Doctor = require('../models/doctor');
const Patient = require('../models/patient');
const Admin = require('../models/admin');
const bcrypt = require('bcryptjs');


let authenticationController = {}

authenticationController.signup = (req, res, next) => {
    console.log("req.body", req.body)

    if (!req.body.userType) {
        req.success = false
        next(null, req.flash('message', 'Enter type of user'))
    }
    else if (!req.body.firstName) {
        req.success = false
        next(null, req.flash('message', 'firstName is required'))
    }
    else if (!req.body.lastName) {
        req.success = false
        next(null, req.flash('message', 'lastName is required'))
    }
    else if (!req.body.email) {
        req.success = false
        next(null, req.flash('message', 'email is required'))
    }
    else if (!req.body.password) {
        req.success = false
        next(null, req.flash('message', 'password is required'))
    }
    else if (!(req.body.password == req.body.pw_confirm)) {
        req.success = false
        next(null, req.flash('message', 'password not match'))
    }
    else {
        console.log("req.body", req.body)
        if (req.body.userType == "Admin") {
            Admin.findOne({ email: req.body.email.toLowerCase() }, (err, user) => {
                if (err) {
                    req.success = false
                    next(null, req.flash('message', 'something went wrong'));
                } else if (user) {
                    req.success = false
                    next(null, req.flash('message', 'Email already exist'));
                } else {
                    req.body.email = req.body.email.toLowerCase()
                    req.body.password = authenticationController.generateHash(req.body.password)
                    let admin = new Admin(req.body)
                    admin.save((err, data) => {
                        if (err) {
                            next(err, req.flash('message', 'something went wrong'));
                        } else {
                            req.success = true
                            next(null, data)
                        }
                    })
                }
            })

        } else if (req.body.userType == "Doctor") {
            Doctor.findOne({ email: req.body.email.toLowerCase() }, (err, user) => {
                if (err) {
                    req.success = false
                    next(null, req.flash('message', 'something went wrong'));
                } else if (user) {
                    req.success = false
                    next(null, req.flash('message', 'Email already exist'));
                } else {
                    req.body.email = req.body.email.toLowerCase()
                    req.body.password = authenticationController.generateHash(req.body.password)
                    let doctor = new Doctor(req.body)
                    doctor.save((err, data) => {
                        if (err) {
                            next(err, req.flash('message', 'something went wrong'));
                        } else {
                            req.success = true
                            next(null, data)
                        }
                    })
                }
            })
        } else if (req.body.userType == "Patient") {
            Patient.findOne({ email: req.body.email.toLowerCase() }, (err, user) => {
                if (err) {
                    req.success = false
                    next(null, req.flash('message', 'something went wrong'));
                } else if (user) {
                    req.success = false
                    next(null, req.flash('message', 'Email already exist'));
                } else {
                    req.body.email = req.body.email.toLowerCase()
                    req.body.password = authenticationController.generateHash(req.body.password)
                    let patient = new Patient(req.body)
                    patient.save((err, data) => {
                        if (err) {
                            next(err, req.flash('message', 'something went wrong'));
                        } else {
                            req.success = true
                            next(null, data)
                        }
                    })
                }
            })
        }
        else {
            req.success = false
            next(null, req.flash('message', 'something went wrong'));
        }
    }
}
authenticationController.login = (req, res, next) => {
    console.log("req.body", req.body)

    if (!req.body.userType) {
        req.success = false
        next(null, req.flash('message', 'Enter type of user'))
    }
    else if (!req.body.email) {
        req.success = false
        next(null, req.flash('message', 'email is required'))
    }
    else if (!req.body.password) {
        req.success = false
        next(null, req.flash('message', 'password is required'))
    }
    else {
        console.log("req.body", req.body)
        if (req.body.userType == "Admin") {
            Admin.findOne({ email: req.body.email.toLowerCase() }).exec((err, data) => {
                if (err) {
                    console.log("err", err)
                    req.success = false
                    next(null, req.flash('message', 'something went wrong'));
                } else if (data) {
                    bcrypt.compare(req.body.password, data.password, (err, isMatch) => {
                        console.log("match", err)
                        if (err) {
                            req.success = false
                            next(null, req.flash('message', 'Internal server error'));
                        } else if (isMatch) {
                            console.log("match login")
                            req.session.key = data;
                            req.success = true
                            req.user = data;
                            next(null, data)
                        } else {
                            req.success = false
                            next(null, req.flash('message', 'Password not match'));
                        }
                    })
                } else {
                    req.success = false
                    next(null, req.flash('message', 'User not exist'));
                }
            })
        } else if (req.body.userType == "Doctor") {
            Doctor.findOne({ email: req.body.email.toLowerCase() }).exec((err, data) => {
                if (err) {
                    console.log(err)
                    req.success = false
                    next(null, req.flash('message', 'something went wrong'));
                } else if (data) {
                    bcrypt.compare(req.body.password, data.password, (err, isMatch) => {
                        console.log(err)
                        if (err) {
                            req.success = false
                            next(null, req.flash('message', 'Internal server error'));
                        } else if (isMatch) {
                            req.session.key = data;
                            req.success = true
                            req.user = data;

                            next(null, data)
                        } else {
                            req.success = false
                            next(null, req.flash('message', 'Password not match'));
                        }
                    })
                } else {
                    req.success = false
                    next(null, req.flash('message', 'User not exist'));
                }
            })
        } else if (req.body.userType == "Patient") {
            Patient.findOne({ email: req.body.email.toLowerCase() }).exec((err, data) => {
                if (err) {
                    req.success = false
                    next(null, req.flash('message', 'something went wrong'));
                } else if (data) {
                    bcrypt.compare(req.body.password, data.password, (err, isMatch) => {
                        if (err) {
                            req.success = false
                            next(null, req.flash('message', 'Internal server error'));
                        } else if (isMatch) {
                            req.session.key = data;
                            req.success = true
                            req.user = data;

                            next(null, data)
                        } else {
                            req.success = false
                            next(null, req.flash('message', 'Password not match'));
                        }
                    })
                } else {
                    req.success = false
                    next(null, req.flash('message', 'User not exist'));
                }
            })
        }
        else {
            req.success = false
            next(null, req.flash('message', 'something went wrong'));
        }
    }
}

/**
 * generate hash password
 */
authenticationController.generateHash = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(9));
}
/**
 * compare hash password
 */
authenticationController.comparePassword = function (passwordToCompare, hashPassword, callback) {
    bcrypt.compare(passwordToCompare, hashPassword, function (err, isMatch) {
        callback(err, isMatch);
    });
};



module.exports = authenticationController;
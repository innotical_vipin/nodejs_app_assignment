const Doctor = require('../models/doctor')


let doctorController = {}

doctorController.getDoctorById = (req, res, next) => {
    if (req.session.key._id) {
        Doctor.findOne({ _id: req.session.key._id }, (err, doctor) => {
            if (err) {
                req.success = false
                next(null, req.flash('message', 'Internal server error'));
            } else {
                req.success = true
                req.info = doctor;
                next(null, doctor)
            }
        })
    } else {
        req.success = false
    }

}

doctorController.getDoctors = (req, res, next) => {
    console.log("req.session", req.session.key)
    if (req.session.key) {
        if (req.session.key.userType == "Admin") {
            Doctor.find({}, (err, doctor) => {
                if (err) {
                    req.success = false
                    next(null, req.flash('message', 'Internal server error'));
                } else {
                    req.success = true
                    req.info = doctor;
                    next(null, doctor)
                }
            })
        } else {
            req.success = false
            next(null)
        }
    }
    else {
        req.success = false
        next(null)
    }

}
doctorController.getDoctor = (req, res, next) => {
        Doctor.findOne({ _id: req.query.doctorId }, (err, doctor) => {
            if (err) {
                req.success = false
                next(null, req.flash('message', 'Internal server error'));
            } else {
                req.success = true
                req.info = doctor;
                next(null, doctor)
            }
        })

}
doctorController.updateInfo=(req,next)=>{
    if (!req.body.userType) {
        req.success = false
        next(null, req.flash('message', 'Enter type of user'))
    }
    else if (!req.body.firstName) {
        req.success = false
        next(null, req.flash('message', 'firstName is required'))
    }
    else if (!req.body.lastName) {
        req.success = false
        next(null, req.flash('message', 'lastName is required'))
    }
    else if (!req.body.email) {
        req.success = false
        next(null, req.flash('message', 'email is required'))
    }else{
        Doctor.update({_id:req.params.id},{$set:req.body},(err,update)=>{
            if(err){
                req.success = false
                next(null, req.flash('message', 'Internal server error'));
            }else{
                req.success = true
                req.info = update;
                next(null, update) 
            }
          
        })  
    }

}

doctorController.getDoctorByAdmin = (req, next) => {
        Doctor.findOne({ _id: req.params.id }, (err, doctor) => {
            if (err) {
                req.success = false
                next(null, req.flash('message', 'Internal server error'));
            } else {
                req.success = true
                req.info = doctor;
                next(null, doctor)
            }
        })
   

}
module.exports = doctorController
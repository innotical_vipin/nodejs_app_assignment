var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
const flash = require('express-flash');
var redis = require("redis");
var session = require('express-session');
var redisStore = require('connect-redis')(session);
var hbs = require('hbs')
var client = redis.createClient();

var app = express();
app.use(session({
    secret: 'ssshhhhh',
    // create new redis store.
    store: new redisStore({ host: 'localhost', port: 6379, client: client, ttl: 26000000 }),
    saveUninitialized: false,
    resave: false
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
hbs.registerHelper('comparison', function (value1, value2, options) {
    console.log("hello helper",value1,value2)
    if (value1 == value2)
        return options.fn(this);
    else
        return options.inverse(this);
})
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(flash());

app.use(function (req, res, next) {
    console.log(typeof req.next);
    next();
});
// catch 404 and forward to error handler
// app.use(function (req, res, next) {
//     var err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });

// // error handler
// app.use(function (err, req, res, next) {
//     // set locals, only providing error in development
//     res.locals.message = err.message;
//     res.locals.error = req.app.get('env') === 'development' ? err : {};

//     // render the error page
//     res.status(err.status || 500);
//     res.render('error');
// });

mongoose.connect('mongodb://localhost/assignment', (err) => {
    if (err)
        console.log("mongodb not connected", err)
    else
        console.log("mongodb connection successfully")

});
app.use(function (req, res, next) {
    res.locals.user = req.session.key;
    next();
});
require('./routes/index')(app)
let server = app.listen(8000)

console.log("server running on port 8000")
app.get('/', function (req, res) {
    let array = ['Doctor', 'Admin', 'Patient']
    res.render('login', {
        info: array
    })
})
const chatController = require('./controllers/chat')
//socket.io instantiation
const io = require("socket.io")(server)
var users = []

//listen on every connection
io.on('connection', (socket) => {
    console.log('New user connected')
    console.log("socketid", socket.id)
    users.push({
        socketId: socket.id
    })
    //default username
    socket.username = "Anonymous"

    //listen on change_username
    socket.on('change_username', (data) => {
        socket.username = data.username
    })

    //listen on new_message
    socket.on('new_message', (data) => {
        console.log("checing", data)
        let post = {}
        if (data.userType == "Admin")
            post.adminId = data.id
        if (data.userType == "Doctor")
            post.doctorId = data.id
        if (data.userType == "Patient")
            post.patientId = data.id
        if (data.senderType == "Admin")
            post.adminId = data.senderId
        if (data.senderType == "Doctor")
            post.doctorId = data.senderId
        if (data.senderType == "Patient")
            post.patientId = data.senderId
        post.msg = data.message
        post.name = data.name
        chatController.createPost(post, (err, doc) => {
            console.log("obj", err, doc)
            socket.broadcast.to(socket.id).emit('new_message', { message: data.message, username: data.name });
            // io.sockets.emit('new_message', { message: data.message, username: data.name });
        })
    })

    //listen on typing
    socket.on('typing', (data) => {
        socket.broadcast.emit('typing', { username: socket.username })
    })
})